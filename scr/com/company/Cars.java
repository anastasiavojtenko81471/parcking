package com.company;

public class Cars extends Places {
    int time;
    int carNumber;

    public Cars(int time, int carNumber) {
        this.time = time;
        this.carNumber = carNumber;

    }

    public int getTime() {
        return time;
    }

    public void setTime() {
        this.time --;
    }

    public int getCarNumber() {
        return carNumber;
    }

}
