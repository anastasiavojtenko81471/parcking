package com.company;

public class Places {
    private Cars currentCar = null;
    private Buses currentBus = null;

    public Cars getCurrentCar() {
        return currentCar;
    }

    public Buses getCurrentBus() {
        return currentBus;
    }

    public boolean setCurrentBus(Buses currentBus) {
        if (this.currentBus == null) {
            this.currentBus = currentBus;
        }
        return this.currentBus == currentBus;
    }

    public boolean setCurrentCar(Cars currentCar) {
        if (this.currentCar == null) {
            this.currentCar = currentCar;
        }
        return this.currentCar == currentCar;
    }

    public boolean isEmptyCar() {
        return this.currentCar == null;
    }

    public boolean isEmptyBus(){
        return this.currentBus == null;
    }

    public void getFreePlaces() {
        this.currentCar = null;
    }

    public void getFreeBusPlaces(){
        this.currentBus = null;
    }
}
