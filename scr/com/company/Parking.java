package com.company;

import java.util.ArrayList;
import java.util.Scanner;

public class Parking {
    private int freeParkingCarPlaces;
    private int freeParkingBusPlaces;
    Scanner in = new Scanner(System.in);
    private final int parkingCarPlaces = in.nextInt();
    private final int parkingBusPlaces = in.nextInt();
    private ArrayList<Places> carPlaces = new ArrayList<>(parkingCarPlaces);
    private ArrayList<Places> busPlaces = new ArrayList<>(parkingBusPlaces);

    public Parking() {
        this.carPlaces = new ArrayList<Places>(parkingCarPlaces);
        this.busPlaces = new ArrayList<Places>(parkingBusPlaces);
        clearCarParking();
        clearBusParking();
    }

    public void setCarOnParkingPlace(Cars car) {
        for (Places a: carPlaces) {
                freeParkingCarPlaces -= 1;
                a.setCurrentCar(car);
                System.out.println("Get car!");
                break;
        }
    }

    public void setBusOnParkingPlace(Buses bus) {
        for (Places b: busPlaces) {
            freeParkingBusPlaces -= 1;
                b.setCurrentBus(bus);
                System.out.println("Get bus!");
                break;
        }
        System.out.println("Why i cant work");
        if (freeParkingBusPlaces == 0) {
            for (int i = 0; i < parkingCarPlaces - 1; i++) {
                if (carPlaces.isEmpty()) {
                    freeParkingCarPlaces -= 2;
                    carPlaces.add(i,bus);
                    carPlaces.add(i+1,bus);
                    System.out.println("Get bus!");
                    break;
                }
            }
        }
    }


    public void deleteCars(Cars car) {
        for (int i = 0; i < parkingCarPlaces; i++) {
            if (!carPlaces.get(i).isEmptyCar() && carPlaces.get(i).getCurrentCar().getTime() == car.getTime()) {
                carPlaces.remove(i);
                freeParkingCarPlaces++;
                break;
            }
        }
    }

    public void deleteBuses(Buses bus) {
        for (int i = 0; i < parkingBusPlaces; i++) {
            if (!busPlaces.get(i).isEmptyBus() && busPlaces.get(i).getCurrentBus().getTime() == bus.getTime()) {
                busPlaces.remove(i);
                freeParkingBusPlaces++;
                break;
            }
        }
    }


    public void getInformation() {
        for (int i = 0; i < parkingCarPlaces; i++) {
            if (!carPlaces.get(i).isEmptyCar()) {
                System.out.println("Car number " + carPlaces.get(i).getCurrentCar().getCarNumber() + " has: " + carPlaces.get(i).getCurrentCar().getTime() + " time");
            }
        }
        for (int i = 0; i < parkingBusPlaces; i++) {
            if (!busPlaces.get(i).isEmptyBus()) {
                System.out.println("Bus number " + busPlaces.get(i).getCurrentBus().getBusNumber() + " has: " + busPlaces.get(i).getCurrentBus().getTime() + " time");
            }
        }
    }

    public void clearCarParking() {
        carPlaces.clear();
        freeParkingCarPlaces = parkingCarPlaces;
    }

    public void clearBusParking() {
        busPlaces.clear();
        freeParkingBusPlaces = parkingBusPlaces;
    }

    public int carCount() {
        return getParkingCarPlaces() - getFreeParkingCarPlaces();
    }

    public int busCount() {
        return getParkingBusPlaces() - getFreeParkingBusPlaces();
    }

    public int getParkingCarPlaces() {
        return parkingCarPlaces;
    }

    public int getParkingBusPlaces() {
        return parkingBusPlaces;
    }

    public int getFreeParkingCarPlaces() {
        return freeParkingCarPlaces;
    }

    public int getFreeParkingBusPlaces() {
        return freeParkingBusPlaces;
    }
}

